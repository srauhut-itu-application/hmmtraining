# Makefile for HMM-Training
# S. Rauhut
# Compiler: g++ (version 9.2.1 20200130)
# Operating system: Arch Linux (kernel: Linux 5.5.8-arch1-1)

CPPCOMP = g++
OPTIONS = -O3 -DNDEBUG
INCLUDE = include
TEST = test

all: hmmtraining tests install clean

# Executable file
hmmtraining: hmmtraining.o
	$(CPPCOMP) -o hmmtraining hmmtraining.o

# Object file
hmmtraining.o : $(INCLUDE)/HMMTrainer.hpp src/hmmtraining.cpp Makefile
	$(CPPCOMP) -c $(OPTIONS) src/hmmtraining.cpp

# Documentation
documentation:
	doxypress doc/hmmtraining-doxypress.json


# Run tests
tests: hmmtraining-demo.o
	$(CPPCOMP) -o hmmtraining-demo hmmtraining-demo.o
	
hmmtraining-demo.o: $(INCLUDE)/HMMTrainer.hpp $(TEST)/hmmtraining-demo.cpp Makefile
	$(CPPCOMP) -c $(OPTIONS) test/hmmtraining-demo.cpp

# Move executable files to bin/ directory
install: hmmtraining
	mv hmmtraining bin/hmmtraining
	mv hmmtraining-demo bin/hmmtraining-demo

# Remove object files
clean:
	rm -f *.o
	

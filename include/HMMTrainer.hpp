// HMMTrainer.hpp
// HMMTrainer trains a Hidden Markov Model
// Based on an annotated text corpus in CoNLL09 format
// S. Rauhut
// Compiler: g++ (version 9.2.1 20200130)
// Operating system: Arch Linux (kernel: Linux 5.5.8-arch1-1)


#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

/// HMMTrainer class responsible for extracting and processing data from the corpus
/// to calculate initial, transition, and emission probabilities
class HMMTrainer
{
public: // Public functions
    HMMTrainer(float smoothing_constant)
    {
        // Smoothing constant specified by user is used for initialization
        init(smoothing_constant);
    }

    /// Function to consolidate all steps of training
    void train(std::string corpus_file, std::string result_file, float smoothing_constant, bool debug_mode)
    {
        // Call function to extract and count all tag and word occurrences in the corpus
        // Store all counts in frequency vectors
        freq_vectors = count_tokens(corpus_file, freq_vectors);
        std::cerr << "Input processed. Calculating probabilities...\n";
        // Call function to calculate probabilities and write results
        // Based on counts stored in frequency vectors
        get_results(result_file, freq_vectors, smoothing_constant, debug_mode);
    }

private: // Private helper classes
    struct Token // Token class consisting of POS tag and emitted word
    {
        std::string word;
        std::string tag;
    };

    struct LexicalEntry // LexicalEntry class
    {
        // Every lexical entry consists of a string and a unique ID
        std::string lexicon_string;
        unsigned lexicon_id;
    };

    struct BinarySearchResult // Data type for results of binary searches on lexicon vectors
    {
        // Consisting of a bool storing whether an entry exists already
        bool entry_exists;
        // And the position in the vector at which an entry occurs or should occur
        // Based on alphabetic order
        unsigned entry_position;
    };

    struct GetIDResult // Date type for results of ID extraction for a lexical entry
    {
        // Consisting of the unique entry ID
        unsigned entry_id;
        // And an updated lexicon vector (if new entry has been inserted)
        std::vector<LexicalEntry> updated_lexicon;
    };

    struct FreqVectorTuple // Class consolidating all data vectors needed for counting of occurrences
    {
        // Two-dimensional vectors for frequencies
        // tag-counts stores frequencies of POS tag bigrams
        std::vector<std::vector<float>> tag_counts;
        // emission_counts stores frequencies of emitted words per POS tag
        std::vector<std::vector<float>> emission_counts;

        // Vector for counts of POS tag unigrams
        std::vector<float> tag_sums;

        // Lexicon vectors for bijective mapping of strings to integers
        // Integers will be used as indices to access count vectors
        std::vector<LexicalEntry> tag_lexicon;
        std::vector<LexicalEntry> word_lexicon;
    };

private: // Private functions
    /// Initialization function that builds all data structures
    void init(float smoothing_constant)
    {
        // Create fixed-size matrices to store absolute frequencies of tag bigrams
        // And emitted words per tag
        // Initialize with smoothing constant so that it is added to all frequencies
        std::vector<std::vector<float>> tag_counts(100, std::vector <float>(100, smoothing_constant));
        std::vector<std::vector<float>> emission_counts(100, std::vector <float>(200000, smoothing_constant));

        // Fixed-size vector storing absolute frequencies of tag bigrams, all initally 0
        std::vector<float> tag_sums(100, 0);

        // Create empty lexicon vectors for tags and words
        // Sized will be increased dynamically as new entries are added
        std::vector<LexicalEntry> tag_lexicon;
        std::vector<LexicalEntry> word_lexicon;

        // Consolidate all vectors in one tuple for easy passing to functions
        freq_vectors = {tag_counts, emission_counts, tag_sums, tag_lexicon, word_lexicon};

        // Special constant token denoting beginning and end of sentence
        BOS_EOS_TOKEN = {"<BOS/EOS>", "$BOS/EOS"};

        // Add tag and word of special token to tag and word lexicon, respectively
        freq_vectors.tag_lexicon = add_entry(freq_vectors.tag_lexicon, BOS_EOS_TOKEN.tag, 0);
        freq_vectors.word_lexicon = add_entry(freq_vectors.word_lexicon, BOS_EOS_TOKEN.word, 0);

        // Increment emission and tag counts for special token
        ++freq_vectors.emission_counts[0][0];
        ++freq_vectors.tag_sums[0];

        // Set current and next line to empty strings
        current_line = "";
        next_line = "";

        // Set current line number to 0
        line_no = 0;
    }

    /// Function for extraction of tag and word from a corpus line
    /// Specific to CoNLL09 format
    Token extract_token(std::string text_line)
    {
        // Set current column in text line to 1 and create new empty token
        unsigned current_column = 1;
        Token new_token;

        // Run through line to build tag and word character-by-character
        for (int i = 0; i < text_line.length(); ++i) {
            char current_char = text_line[i];

            // In CoNNL09 format, columns are always separated by tabs
            if (current_char == '\t') {
                ++current_column;
            }
            // Second column always contains the word, fifth column always contains the POS tag
            else {
                if (current_column == 2) {
                    new_token.word += current_char;
                }
                else if (current_column == 5) {
                    new_token.tag += current_char;
                }
                // All data after fifth column is irrelevant for the HMM
                // Thus stop when sixth column is reached
                else if (current_column == 6) {
                    return new_token;
                }
            }
        }
        // Return extracted token consisting of word and POS tag
        return new_token;
    }

    /// Function that counts all occurrences in a corpus
    FreqVectorTuple count_tokens(std::string corpus_file, FreqVectorTuple freq_vectors)
    {
        // Open corpus file
        std::ifstream corpus_input(corpus_file.c_str());

        // First token is always Beginning of Sentence with ID 0
        current_token = BOS_EOS_TOKEN;
        current_tag_id = 0;

        // Iterate over corpus text
        while (corpus_input) {
            std::getline(corpus_input, next_line);
            // Increment current line number
            ++line_no;
            // After every 1000 lines, print status message to console
            if (line_no % 1000 == 0) {
                std::cerr  << line_no << " lines processed.\n";
            }

            // In CoNNL09 format, empty lines denote end of sentence
            if (next_line == "") {
                next_token = BOS_EOS_TOKEN;
            }
            else { // Extract new token from non-empty line
                next_token = extract_token(next_line);
            }

            // For extracted tag and word, get entry IDs in lexicon vectors
            GetIDResult tag_id_result = get_entry_id(freq_vectors.tag_lexicon, next_token.tag);
            GetIDResult word_id_result = get_entry_id(freq_vectors.word_lexicon, next_token.word);

            // Store updated lexicon vectors
            freq_vectors.tag_lexicon = tag_id_result.updated_lexicon;
            freq_vectors.word_lexicon = word_id_result.updated_lexicon;

            // Store IDs of next tag and next word
            next_tag_id = tag_id_result.entry_id;
            next_word_id = word_id_result.entry_id;

            // Increment frequencies of tags and words, using their IDs as entry indices
            freq_vectors = update_freqs(freq_vectors, current_tag_id, next_tag_id, next_word_id);

            // Next token becomes new current token
            current_token = next_token;
            current_tag_id = next_tag_id;
            current_word_id = next_word_id;

            // Next line becomes current line
            current_line = next_line;
        }
        // When all input has been processed, close corpus file and return finished frequency vectors
        corpus_input.close();
        return freq_vectors;
    }

    /// Function that adds a new entry to a lexicon vector
    std::vector<LexicalEntry> add_entry(std::vector<LexicalEntry> lexicon_vector, std::string search_string, unsigned entry_position)
    {
        // Every entry is a tuple of custom type LexicalEntry
        // Containing string and unique ID
        // Create empty entry
        LexicalEntry new_lexical_entry;

        // Requested string becomes lexical entry string
        new_lexical_entry.lexicon_string = search_string;
        // Unique ID is determined by current number of entries already in lexicon
        new_lexical_entry.lexicon_id = lexicon_vector.size();

        // Insert new entry at specified entry position
        lexicon_vector.insert(lexicon_vector.begin() + entry_position, new_lexical_entry);

        // Return updated lexicon vector
        return lexicon_vector;
    }

    /// Function that gets the ID from a lexical entry
    GetIDResult get_entry_id(std::vector<LexicalEntry> lexicon_vector, std::string search_string)
    {
        // Create empty result tuple consisting of ID and new lexicon vector
        GetIDResult get_id_result;

        // Run binary search on lexicon vector to find the search string
        BinarySearchResult binsearch_result = binary_search(lexicon_vector, search_string, 0, lexicon_vector.size()-1);

        // If entry exists already, no change is made to the lexicon vector
        // And the entry ID is simply extracted
        if (binsearch_result.entry_exists) {
            get_id_result.updated_lexicon = lexicon_vector;
            // Access lexicon vector using position returned by binary search as index
            // Then get the ID from the entry at that position
            get_id_result.entry_id = lexicon_vector[binsearch_result.entry_position].lexicon_id;
        }
        // If entry does not yet exist, add it to the lexicon vector, then get the new ID
        else {
            get_id_result.updated_lexicon = add_entry(lexicon_vector, search_string, binsearch_result.entry_position);
            get_id_result.entry_id = get_id_result.updated_lexicon[binsearch_result.entry_position].lexicon_id;
        }

        // Return result containing ID and updated lexicon vector
        return get_id_result;
    }

    /// Recursive function that performs a binary search on a vector
    /// Based on algorithm proposed by Aho & Ullman (1992)
    BinarySearchResult binary_search(std::vector<LexicalEntry> lexicon_vector, std::string search_string, int lowerbound, int upperbound)
    {
        // Create empty result tuple
        BinarySearchResult binsearch_result;

        // Base case: remaining vector is empty
        if (lowerbound > upperbound) {
            // Entry has not been found, lower bound becomes position for new entry
            binsearch_result.entry_exists = false;
            binsearch_result.entry_position = lowerbound;
        }
        else {
            // Calculate center position of current vector
            int middle = int((lowerbound + upperbound)/2);
            // If the entry at the center position matches the search string, the search is finished
            if (lexicon_vector[middle].lexicon_string == search_string) {
                binsearch_result.entry_exists = true;
                binsearch_result.entry_position = middle;
            }
            // If the entry at the center position is alphabetically before the search string
            // Perform another binary search on just the second half of the vector
            else if (lexicon_vector[middle].lexicon_string < search_string) {
                binsearch_result = binary_search(lexicon_vector, search_string, middle + 1, upperbound);
            }
            else { // Otherwise perform another binary search on just the first half
                binsearch_result = binary_search(lexicon_vector, search_string, lowerbound, middle - 1);
            }
        }
        // After the search is finished, return the result
        return binsearch_result;
    }

    /// Function that takes the IDs of tags and words
    /// And increments their frequencies stored in given frequency vectors
    FreqVectorTuple update_freqs(FreqVectorTuple freq_vectors, unsigned current_tag_id, unsigned next_tag_id, unsigned next_word_id)
    {
        // Increment count for current tag bigram
        ++freq_vectors.tag_counts[current_tag_id][next_tag_id];
        // Increment count for current tag-word pair
        ++freq_vectors.emission_counts[next_tag_id][next_word_id];
        // Increment total count for current tag unigram
        ++freq_vectors.tag_sums[next_tag_id];

        // Return updated vectors in a tuple
        return freq_vectors;
    }

    /// Function that writes the header of the result file after all counts have been determined
    void write_header(std::string result_file, FreqVectorTuple freq_vectors, float smoothing_constant)
    {
        // Open result file with filename specified by the user
        std::ofstream results_out;
        results_out.open(result_file);
        results_out << "[General]\n";
        // Number of states (i. e. tags) is the number of entries in final tag lexicon vector, save the BOS tag
        results_out << "Number of states: " << freq_vectors.tag_lexicon.size()-1 << "\n";
        // Number of words is the number of entries in final word lexicon vector, save the BOS word
        results_out << "Number of words: " << freq_vectors.word_lexicon.size()-1 << "\n";
        // Smoothing constant as specified by the user
        results_out << "Smoothing constant: " << smoothing_constant << "\n\n";
    }

    /// Function that calculates and writes initial probabilities
    /// All in one go so the probabilities do not have to be stored
    void write_initial(std::string result_file, FreqVectorTuple freq_vectors, float smoothing_constant, bool debug_mode)
    {
        // Open result file in append mode
        std::ofstream results_out;
        results_out.open(result_file, std::ios::app);

        // Set initial sum of all initial probabilities to 0
        double initial_prob_sum = 0;

        results_out << "[Initial]\n";

        // Iterate over all entries in the tag lexicon
        for (int i = 0; i < freq_vectors.tag_lexicon.size(); ++i) {
            // For each entry, calculate the probability of occurring directly after BOS
            // BOS frequencies are stored in the first line of the vector
            // Access the count by using the tag ID as entry index
            // And divide the count by the sum of all BOS counts
            // While applying the smoothing constant directly to the sum
            double initial_prob =
                freq_vectors.tag_counts[0][freq_vectors.tag_lexicon[i].lexicon_id]
                /(freq_vectors.tag_sums[0] -1 + smoothing_constant * freq_vectors.tag_lexicon.size());
            // Write results to result file
            results_out << freq_vectors.tag_lexicon[i].lexicon_string << "\t" << initial_prob << "\n";
            // If in debug mode, also add up the individual probabilities
            if (debug_mode) {
                initial_prob_sum += initial_prob;
            }
        }
        // If in debug mode, print the sum of all initial probabilities
        // Sum should always be 1
        if (debug_mode) {
            std::cerr << "Sum of initial probabilities is " << initial_prob_sum << "\n";
        }
        results_out << "\n";
    }

    /// Function that calculates and writes transition probabilities
    void write_transition(std::string result_file, FreqVectorTuple freq_vectors, float smoothing_constant, bool debug_mode)
    {
        // Open result file in append mode
        std::ofstream results_out;
        results_out.open(result_file, std::ios::app);

        results_out << "[Transition]\n";

        // Iterate over all entries in the tag lexicon
        for (int i = 0; i < freq_vectors.tag_lexicon.size(); ++i) {
            // Except the BOS token, as this is a special case already treated in the initial probabilities
            if (freq_vectors.tag_lexicon[i].lexicon_id != 0) {
                // For every tag, set the sum of all transition probabilities initially to 0
                double transition_prob_sum = 0;
                // Iterate over all entries in the tag lexicon again
                for (int j = 0; j < freq_vectors.tag_lexicon.size(); ++j) {
                    // Calculate transition frequency by accessing count vectors using entry IDs
                    // Then dividing the count stored in the vector by the sum of all occurrences of the tag
                    // While applying smoothing constant directly to the sum of all occurrences
                    double transition_prob =
                        freq_vectors.tag_counts[freq_vectors.tag_lexicon[i].lexicon_id][freq_vectors.tag_lexicon[j].lexicon_id]
                        /(freq_vectors.tag_sums[freq_vectors.tag_lexicon[i].lexicon_id]
                            + smoothing_constant * freq_vectors.tag_lexicon.size());
                    // Write to result file
                    results_out << freq_vectors.tag_lexicon[i].lexicon_string << "\t" << freq_vectors.tag_lexicon[j].lexicon_string << "\t" << transition_prob << "\n";
                    // If in debug mode, also add up the individual probabilities for each tag
                    if (debug_mode) {
                        transition_prob_sum += transition_prob;
                    }
                }
                // If in debug mode, print the sum of all transition probabilities for each tag
                // Sums should always be 1
                if (debug_mode) {
                    std::cerr << "Sum of transition probabilities for " << freq_vectors.tag_lexicon[i].lexicon_string << " is " << transition_prob_sum << "\n";
                }
            }
        }

        results_out << "\n";
    }

    /// Function that calculates and writes emission probabilities
    void write_emission(std::string result_file, FreqVectorTuple freq_vectors, float smoothing_constant, bool debug_mode)
    {
        // Open result file in append mode
        std::ofstream results_out;
        results_out.open(result_file, std::ios::app);

        results_out << "[Emission]\n";

        // Iterate over all entries in the tag lexicon, except the special BOS/EOS token
        for (int i = 0; i < freq_vectors.tag_lexicon.size(); ++i) {
            if (freq_vectors.tag_lexicon[i].lexicon_id != 0) {
                // For every tag, set the sum of all transition probabilities initially to 0
                double emission_prob_sum = 0;
                // Iterate over all entries in the word lexicon
                for (int j = 0; j < freq_vectors.word_lexicon.size(); ++j) {
                    // Calculate emission frequency by accessing count vectors using entry IDs
                    // Then dividing the count stored in the vector by the sum of all occurrences of the tag
                    // While applying smoothing constant directly to the sum of all occurrences
                    double emission_prob =
                        freq_vectors.emission_counts[freq_vectors.tag_lexicon[i].lexicon_id][freq_vectors.word_lexicon[j].lexicon_id]
                        /(freq_vectors.tag_sums[freq_vectors.tag_lexicon[i].lexicon_id]
                            + smoothing_constant * freq_vectors.word_lexicon.size());
                    // Write to result file
                    results_out << freq_vectors.tag_lexicon[i].lexicon_string << "\t" << freq_vectors.word_lexicon[j].lexicon_string << "\t" << emission_prob << "\n";
                    // If in debug mode, also add up the individual probabilities for each tag
                    if (debug_mode) {
                        emission_prob_sum += emission_prob;
                    }
                }
                // If in debug mode, print the sum of all emission probabilities for each tag
                // Sums should always be 1
                if (debug_mode) {
                    std::cerr << "Sum of emission probabilities for " << freq_vectors.tag_lexicon[i].lexicon_string << " is " << emission_prob_sum << "\n";
                }
            }
        }
        // Finally close the result file
        results_out.close();
    }

    /// Function that writes all four paragraphs into result file
    void get_results(std::string result_file, FreqVectorTuple freq_vectors, float smoothing_constant, bool debug_mode)
    {
        write_header(result_file, freq_vectors, smoothing_constant);
        write_initial(result_file, freq_vectors, smoothing_constant, debug_mode);
        write_transition(result_file, freq_vectors, smoothing_constant, debug_mode);
        write_emission(result_file, freq_vectors, smoothing_constant, debug_mode);
    }

private: // Instance variable
    /// Tuple consolidating all five vectors used to count frequencies
    FreqVectorTuple freq_vectors;

    /// Special token denoting beginning and end of sentences
    Token BOS_EOS_TOKEN;

    /// Token extracted from current line in corpus
    Token current_token;
    /// Token extracted from next line in corpus
    Token next_token;

    /// Line currently read from corpus
    std::string current_line;
    /// Next line read from corpus
    std::string next_line;

    /// Current number of line from corpus
    unsigned line_no;

    /// ID number of current POS tag
    unsigned current_tag_id;
    /// ID number of current word
    unsigned current_word_id;
    /// ID number of next POS tag
    unsigned next_tag_id;
    /// ID number of next word
    unsigned next_word_id;
}; // HMMTrainer

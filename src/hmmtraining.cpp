// hmmtraining.cpp
// Main program for the HMM Training program that trains a Hidden Markov Model
// Based on a text corpus in CoNLL09 format
// S. Rauhut
// Compiler: g++ (version 9.2.1 20200130)
// Operating system: Arch Linux (kernel: Linux 5.5.8-arch1-1)


#include <iostream>
#include <fstream>
#include <string>

// Include HMMTrainer header file
#include "../include/HMMTrainer.hpp"

/// Tuple consolidating command line arguments
struct CmdArguments
{
    std::string corpus_file; // Input file containing corpus data
    std::string result_file; // Output file for training results
    float smoothing_constant; // Smoothing constant probability calculations
    bool debug_mode; // Optional debug mode
};

/// Synopsis function for usage information
void synopsis()
{
    std::cerr << "Synopsis: hmmtraining [--debug] SMOOTHING_CONSTANT CORPUS_FIlE.conll09 OUTPUT_FILE\n";
    std::cerr << "\tSMOOTHING_CONSTANT is a non-negative floating-point number;\n";
    std::cerr << "\tCORPUS_FILE is a text file containing the corpus data in CoNLL09 format;\n";
    std::cerr << "\tOUTPUT_FILE is the will contain the training results;\n";
    std::cerr << "\tUse the --debug option to activate debug mode.\n";
}

/// Function for testing whether input file exists
bool file_exists(std::string input_file)
{
    std::ifstream open_inputfile(input_file.c_str());
    return bool(open_inputfile);
}

/// Function that extracts command line arguments from user input
CmdArguments parse_cmdline(int argc, char **argv)
{
    CmdArguments cmdargs;
    // User input has to specify four or five arguments
    if (argc != 4 && argc != 5) {
        std::cerr << "ERROR: Incorrect number of arguments\n";
        synopsis();
        exit(1);
    }
    else if (argc == 4) { // In case of four arguments, extract parameters
        cmdargs.smoothing_constant = atof(argv[1]); // Convert smoothing constant to float
        cmdargs.debug_mode = false; // Debug mode has not been activated
        cmdargs.corpus_file = argv[2];
        cmdargs.result_file = argv[3];
    }
    else if (argc == 5) { // In case of five arguments, second argument has to be --debug
        if (std::string(argv[1]) != "--debug") {
            std::cerr << "ERROR: Bad arguments\n";
            synopsis();
            exit(1);
        }
        else { // If second argument is debug, extract parameters
            cmdargs.smoothing_constant = atof(argv[2]); // Convert smoothing constant to float
            cmdargs.debug_mode = true; // Set debug mode to true
            cmdargs.corpus_file = argv[3];
            cmdargs.result_file = argv[4];
        }
    }

    // Return struct of all extracted parameters
    return cmdargs;
}

/// Main function: parses command line input and starts HMM training
int main(int argc, char **argv)
{
    // Extract parameters from command line input
    CmdArguments cmdargs = parse_cmdline(argc, argv);

    // Check if corpus file exists
    if (!file_exists(cmdargs.corpus_file)) {
        std::cerr << "ERROR: Unable to open '" << cmdargs.corpus_file << "'\n";
        synopsis();
        exit(2);
    }

    // Check if smoothing constant is non-negative
    if (cmdargs.smoothing_constant < 0) {
        std::cerr << "ERROR: Smoothing constant cannot be negative\n";
        synopsis();
        exit(2);
    }

    // Instantiate HMMTrainer class with smoothing constant
    HMMTrainer hmm_trainer(cmdargs.smoothing_constant);

    // Call train function of HMMTrainer class using parameters from command line input
    hmm_trainer.train(cmdargs.corpus_file, cmdargs.result_file, cmdargs.smoothing_constant, cmdargs.debug_mode);

    exit(0);
}

#!/bin/bash

# Use this shell script to get the number of unique words and unique POS tags in a CoNNL09-formatted corpus file

# Extract second column from CoNNL09 file
# And get number of unique lines
echo "Number of unique words: "
awk '{print $2}' $1 | sort | uniq | wc -l


# Extract fifth column from CoNNL09 file
# And get number of unique lines
echo "Number of unique POS tags: "
awk '{print $5}' $1 | sort | uniq | wc -l

 // hmmtraining-demo.cpp
 // Demo program for the HMM Training tool
 // Runs the program with example input
 // S. Rauhut
 // Compiler: g++ (version 9.2.1 20200130)
 // Operating system: Arch Linux (kernel: Linux 5.5.8-arch1-1)

#include "../include/HMMTrainer.hpp"

bool file_exists(std::string input_file)
{
    std::ifstream open_inputfile(input_file.c_str());
    return bool(open_inputfile);
}

int main()
{
    std::cerr << "Starting automated tests...\n";

    // Test Block 1: with properly formatted input files
    std::cerr << "----- Test 1: Proper corpus files without smoothing:\n";

    std::cerr << "--- Test 1a: Using test-corpus1.conll09:\n";
    if (!file_exists("../data/test-corpus1.conll09")) {
        std::cerr << "ERROR: Unable to open test-corpus1.conll09\n";
        exit(1);
    }
    HMMTrainer test_trainer1a(0);
    test_trainer1a.train("../data/test-corpus1.conll09", "../test/test-result-1a.txt", 0, true);
    std::cerr << "> Test 1a passed.\n\n";

    std::cerr << "--- Test 1b: Using test-corpus2.conll09:\n";
    if (!file_exists("../data/test-corpus2.conll09")) {
        std::cerr << "ERROR: Unable to open test-corpus2.conll09\n";
        exit(1);
    }
    HMMTrainer test_trainer1b(0);
    test_trainer1b.train("../data/test-corpus2.conll09", "../test/test-result-1b.txt", 0, true);
    std::cerr << "> Test 1b passed.\n\n";

    std::cerr << "----- Test 2: Proper corpus files with smoothing:\n";

    std::cerr << "--- Test 2a: Using test-corpus1.conll09:\n";
    if (!file_exists("../data/test-corpus1.conll09")) {
        std::cerr << "ERROR: Unable to open test-corpus1.conll09\n";
        exit(1);
    }
    HMMTrainer test_trainer2a(0.1);
    test_trainer2a.train("../data/test-corpus1.conll09", "../test/test-result-2a.txt", 0.1, true);
    std::cerr << "> Test 2a passed.\n\n";

    std::cerr << "--- Test 2b: Using test-corpus2.conll09:\n";
    if (!file_exists("../data/test-corpus2.conll09")) {
        std::cerr << "ERROR: Unable to open test-corpus2.conll09\n";
        exit(1);
    }
    HMMTrainer test_trainer2b(0.1);
    test_trainer2b.train("../data/test-corpus2.conll09", "../test/test-result-2b.txt", 0.1, true);
    std::cerr << "> Test 2b passed.\n\n";

    std::cerr << "----- Test 3: Invalid or empty corpus files:\n";

    std::cerr << "--- Test 3a: Using empty corpus file:\n";
    if (!file_exists("../data/empty-test-corpus.txt")) {
        std::cerr << "ERROR: Unable to open empty-test-corpus.txt\n";
        exit(1);
    }

    HMMTrainer test_trainer3a(0);
    test_trainer3a.train("../data/empty-test-corpus.txt", "../test/test-result-3a.txt", 0, true);
    std::cerr << "> Test 3a passed.\n\n";

    std::cerr << "--- Test 3b: Using invalid corpus file:\n";
    if (!file_exists("../data/wrong-format-corpus.txt")) {
        std::cerr << "ERROR: Unable to open wrong-format-corpus.txt\n";
        exit(1);
    }
    HMMTrainer test_trainer3b(0);
    test_trainer3b.train("../data/wrong-format-corpus.txt", "../test/test-result-3b.txt", 0, true);
    std::cerr << "> Test 3b passed.\n\n";

    // Test Block 4: Stress test with full TIGER corpus
    // WARNING: ~1M lines
    /*
    if (!file_exists("../data/tiger_release_aug07.corrected.16012013.conll09")) {
        std::cerr << "ERROR: Unable to open tiger_release_aug07.corrected.16012013.conll09\n";
        exit(1);
    }
    HMMTrainer test_trainer4(0.1);
    test_trainer4.train("../data/tiger_release_aug07.corrected.16012013.conll09", "../test/test-result-4.txt", 0.1, true);
    std::cerr << "> Test 4 passed.\n\n";
    */
}
